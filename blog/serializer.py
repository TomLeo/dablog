from rest_framework import serializers

from blog.models import Post


class PostSerializer(serializers.ModelSerializer):
    url = serializers.CharField(source='get_absolute_url', read_only=True)

    class Meta:
        model = Post
        fields = ['author', 'title', 'slug', 'published', 'intro', 'content', 'url']
