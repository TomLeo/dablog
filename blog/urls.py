from django.conf.urls import url

from blog.views import PostDetailView, PostListView, PostCreateView

urlpatterns = [
    url('^$', PostListView.as_view(), name='post-list'),
    url('^(?P<pk>\d+)/$', PostDetailView.as_view(), name='post-detail'),
    url('^new/$', PostCreateView.as_view(), name='post-new')
]
