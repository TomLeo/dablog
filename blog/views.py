from rest_framework import renderers, generics
from rest_framework.response import Response
from blog.models import Post
from blog.serializer import PostSerializer


class PostListView(generics.ListAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    renderer_classes = [
        renderers.TemplateHTMLRenderer,
        renderers.AdminRenderer,
        renderers.JSONRenderer,
    ]

    def get(self, request, *args, **kwargs):
        posts = self.get_queryset().order_by('-published')
        return Response({'posts': posts}, template_name='post_list.html')


class PostDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    template_name = 'post_detail.html'
    renderer_classes = [
        renderers.TemplateHTMLRenderer,
        renderers.AdminRenderer,
        renderers.JSONRenderer
    ]


class PostCreateView(generics.CreateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    template_name = 'post_new.html'
    renderer_classes = [
        renderers.TemplateHTMLRenderer,
        renderers.AdminRenderer,
        renderers.JSONRenderer
    ]

    def get(self, request, *args, **kwargs):
        return Response({'serializer': self.serializer_class})
